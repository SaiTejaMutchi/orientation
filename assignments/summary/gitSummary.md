## Git
- Git is version-control software that makes collaboration with teammates super simple.
- Git lets you easily keep track of every revision you and your team make during the development of your software.
- Git is an Open Source Distributed Version Control System.
### Terms we use in Git
- Repository: A repository is the collection of files and folders (code files) that you’re using git to track. It’s the big box you and your team throw your code into.
- The process of copying an existing Git repository via the Git tooling is called cloning.Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.
- A local repository provides at least one collection of files which originate from a certain version of the repository. This collection of files is called the working tree.
- Remote Repository : This is the copy of your Local Repository but is stored in some
server on the Internet. All the changes you commit into the Local Repository are
not directly reflected into this tree. You need to push your changes to the Remote
Repository.
- Workspace : All the changes you make via Editor(s) (gedit, notepad, vim, nano) is
done in this tree of repository.
- All the staged files go into this tree of your repository.
- After modifying your working tree you need to perform the following two steps to persist these changes in your local repository:
  - add the selected changes to the staging area (also known as index) via the git add command
  - commit the staged changes into the Git repository via the git commit command

  ![alt text](https://gitlab.com/SaiTejaMutchi/orientation/-/raw/master/extras/4.png)
- Git supports branching which means that you can work on different versions of your collection of files. A branch allows the user to switch between these versions so that he can work on different changes independently from each other.
- When a branch is free of bugs (as far as you can tell, at least), and ready to become part of the primary codebase, it will get merged into the master branch. Merging is just what it sounds like: integrating two branches together.
- When you commit to a repository, it’s like you’re taking picture/snapshot of the files as they exist at that moment. The commit will only
exist on your local machine until it is pushed to a remote repository.Where as pushing is essentially syncing your commits to GitLab.
  ![alt text](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)
### Workflow
 -  clone the repo
 -  Create a new branch
 -  modify files in your working tree.
 -  selectively stage just those changes you want to be part of your next commit,
       which adds only those changes to the staging area.
 -  do a commit, which takes the files as they are in the staging area and stores that
      snapshot permanently to your Local Git Repository.
 -  do a push, which takes the files as they are in the Local Git Repository and stores
that snapshot permanently to your Remote Git Repository  

### Basic commands
- Create  new local repository
 > git init
-  Create a working copy of a local repository
 > git clone /path/to/repository
- Add file
 > git add <*filename*>
- Commit changes <>
 > git commit -sv
- Push changes to remote repository
 > git push origin <*branch-name*>
- Status
 > git status
- Create a new branch and switch to it
 > git checkout -b <*branch-name*>
- Clone   
 > git clone <*link-to-repository*> 
