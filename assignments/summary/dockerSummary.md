## Docker
- Docker is a platform for developers and sysadmins to build, run, and share applications with containers. The use of containers to deploy applications is called containerization. 
- A container is nothing but a running process, with some added encapsulation features applied to it in order to keep it isolated from the host and from other containers.
- Docker image includes everything needed to run an application - the code or binary, runtimes, dependencies, and any other filesystem objects required.
- The image can then be deployed to any Docker environment and as a container.
- A Docker container is a running Docker image.From one image you can create multiple containers .
 ![Docker deploys containers at all layers of the hybrid cloud.](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/container-docker-introduction/media/docker-defined/docker-containers-run-anywhere.png)
- Docker containers can run anywhere, on-premises in the customer datacenter, in an external service provider or in the cloud, on Azure
- Developers can use development environments on Windows, Linux, or macOS. On the development computer, the developer runs a Docker host where Docker images are deployed, including the app and its dependencies. Developers who work on Linux or on macOS use a Docker host that is Linux based, and they can create images only for Linux containers. (Developers working on macOS can edit code or run the Docker CLI from macOS, but as of the time of this writing, containers don't run directly on macOS.) Developers who work on Windows can create images for either Linux or Windows Containers.
- Containerization is an approach to software development in which an application or service, its dependencies, and its configuration (abstracted as deployment manifest files) are packaged together as a container image. The containerized application can be tested as a unit and deployed as a container image instance to the host operating system (OS).
- software containers act as a standard unit of software deployment that can contain different code and dependencies. Containerizing software this way enables developers and IT professionals to deploy them across environments with little or no modification.
- Containers also isolate applications from each other on a shared OS. Containerized applications run on top of a container host that in turn runs on the OS (Linux or Windows). Containers therefore have a significantly smaller footprint than virtual machine (VM) images.
- Each container can run a whole web application or a service.  
![](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/container-docker-introduction/media/index/multiple-containers-single-host.png)
- The most important benefit is the environment's isolation provided between Dev and Ops.
### Operations on Docker
1. Download the docker: 
>docker pull snehabhapkar/trydock
2. Run the docker image with this command.
> docker run -ti snehabhapkar/trydock /bin/bash  
  - gives your id.
3. copy our code inside docker with this command.
> hello.py file : print("hello, I am talking from container")  
>docker cp hello.py (e0b72.....):/
- In (e0b72...) there should be your id.
- write a script for installing dependencies - requirements.sh
    > apt update
    > apt install python3
- Copy file inside docker
    > docker cp requirements.sh e0b72ff850f8:/
4. Install dependencies
-  Give permission to run shell script
 > docker exec -it e0b72ff850f8 chmod +x requirements.sh
- Install dependencies
 > docker exec -it e0b72ff850f8 /bin/bash ./requirements.sh
5. Run the program inside container with this command.
 > docker start e0b72ff850f8
 > docker exec e0b72ff850f8 python3 hello.py
- In all this example e0b72ff850f8 is the id. you have put id you have got in first step
6. Save your copied program inside docker image with docker commit.
 > docker commit e0b72ff850f8 snehabhapkar/trydock 
7. Push docker image to the dockerhub.
- Tag image name with different name
 > docker tag snehabhapkar/trydock username/repo
- Push
 > docker push username/repo

